<?php

/**
 * A program that prints the numbers from 1 to 100
 * For multiples of three print “Fizz” instead of the number
 * and for the multiples of five print “Buzz”.
 * For numbers which are multiples of both three and five print “FizzBuzz”."
 **/
class FizzBuzz
{
    private static $fizz = "Fizz";
    private static $buzz = "Buzz";
    private static $fizzBuzz = "FizzBuzz";

    public static function solve($n)
    {
        if ($n%3 == 0 && $n%5 == 0)
            return self::$fizzBuzz;
        
        if ($n%3 == 0)
            return self::$fizz;

        if ($n%5 == 0)
            return self::$buzz;
        
        return $n;
    }
}

class Controller
{
    public static function main($n)
    {
        $result = FizzBuzz::solve($n);
        header("Location:index.php?result=$result");
    }
}

if ( ! empty($_POST['n'])) {
    Controller::main($_POST['n']);
}
?>