<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>1-4 (FizzBuzz)</title>
</head>
<body>
    <div>
        <form action="controller.php"
            method="POST">
            <div class="form-group-input">
                <input
                    type="number"
                    name="n"
                    required
                />
            </div>

            <input type="submit"
                value="Submit"
            />
        </form>

        <?php if ( ! empty($_GET['result'])): ?>
            <div class="result">
                RESULT = <?=$_GET['result']?>
            </div>
        <?php endif; ?>
    </div>
</body>
</html>