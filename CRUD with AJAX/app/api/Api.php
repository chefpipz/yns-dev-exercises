<?php

namespace api;

class Api
{
    protected static function response ($status = 200, $data = [])
    {
        http_response_code($status);
        echo json_encode($data);
    }

    protected static function responseOK ($data = [])
    {
        self::response(200, $data);
    }

    protected static function responseError ($errors = [], $status = 400)
    {
        self::response(400, $errors);
    }

    protected static function responseCreated ()
    {
        self::response(201);
    }

    protected static function responseNoContent ()
    {
        self::response(204);
    }

    protected static function responseNotFound ()
    {
        self::response(404);
    }

    protected static function responseServerError ()
    {
        self::response(500);
    }
}