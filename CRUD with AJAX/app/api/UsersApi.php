<?php

namespace api;

use models\User;
use services\UserService;
use validations\CreateUserValidation;

class UsersApi extends Api
{
    /**
     * GET
     * handles the fetching of users
     * 
     * @return json
     */
    public static function fetchUserList()
    {
        $page = isset($_GET['page']) && ! empty($_GET['page'])
            ? $_GET['page']
            : 1;
        $userService = new UserService();
        $users = $userService->paginateUser($page);
        self::responseOK($users);
    }

    /**
     * POST
     * TODO move validation to a Custom Request
     * handles the creation of the user
     * 
     * @param router\Request $request
     * @return json
     */
    public static function createUser($request)
    {
        $data = $request->getBody();
        try {
            $validation = new CreateUserValidation($data);
            if ( ! $validation->isValid) {
                return self::responseError($validation->errors);
            }
            $userService = new UserService();
            $userService->createUser($data);

            return self::responseCreated();
        } catch (Exception $e) {
            return self::responseServerError();
        }
    }

    /**
     * PUT
     * TODO move validation to a Custom Request
     * handles the updating of user
     * 
     * @param router\Request $request
     * @return json
     */
    public static function updateUser($request)
    {
        if ( ! self::doesIdExist()) {
            return self::responseNotFound();
        }
        try {
            $data = $request->getBody();
            $validation = new CreateUserValidation($data);
            if ( ! $validation->isValid) {
                return self::responseError($validation->errors);
            }
            $userService = new UserService();
            $userService->updateUser($_GET['id'], $data);
            return self::responseOK();
        } catch (\Exception $e) {
            return self::responseServerError();
        }
    }

    /**
     * DELETE
     * handles the deletion of a user
     */
    public static function deleteUser($request)
    {
        if ( ! self::doesIdExist()) {
            return self::responseNotFound();
        }

        try {
            $userService = new UserService();
            $userService->deleteUser($_GET['id']);
            return self::responseNoContent();
        } catch (\Exception $e) {
            return self::responseServerError();
        }
    }
    
    private static function doesIdExist()
    {
        if ( ! $_GET['id'] && empty($_GET['id'])) {
            return false;
        }
        $user = new User($_GET['id']);
        if ( ! $user->fetchById()->row()) {
            return false;
        }

        return true;
    }
}