<?php

namespace controllers;

class UsersController
{
    /** VIEWS */
    public static function list($request)
    {
        $request->display('components/user/user-list.html');
    }
}