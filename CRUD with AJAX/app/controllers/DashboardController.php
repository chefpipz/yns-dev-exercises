<?php

namespace controllers;

class DashboardController
{
    /** VIEWS */
    public static function index($request)
    {
        $request->display('components/dashboard/dashboard.php');
    }
}