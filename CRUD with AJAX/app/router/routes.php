<?php

/*************************
            VIEWS
*************************/
$ROUTER->get('/users', 'controllers\UsersController::list');
$ROUTER->get('/', 'controllers\DashboardController::index');

/*************************
            API
*************************/

/******** USERS *********/
$userURL = '/api/users';
/** GET */
$ROUTER->get("$userURL", 'api\UsersApi::fetchUserList');

/** POST */
$ROUTER->post("$userURL/create", 'api\UsersApi::createUser');

/** PUT */
$ROUTER->put("$userURL", 'api\UsersApi::updateUser');

/** DELETE */
$ROUTER->delete("$userURL", 'api\UsersApi::deleteUser');