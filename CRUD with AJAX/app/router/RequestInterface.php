<?php

interface RequestInterface
{
    public function getBody();
    public function validate();
}