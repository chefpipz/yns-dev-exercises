<?php

require_once "RequestInterface.php";

use helpers\View;
use helpers\ViewLoader;

class Request implements RequestInterface
{
    private $queryStrings = '';

    private $view;

    function __construct()
    {
        $this->bootstrapSelf();
        $this->view = new View(new ViewLoader(BASEPATH . '/app/views/'));
    }

    public function display($pathName, $data = []) {
        $this->view->display($pathName, $data);
    }

    public function validate() {}

    public function setQueryStrings(String $queryStrings) {
        $this->queryStrings = $queryStrings;
    }
    public function getQueryStrings() { return $this->queryStrings; }

    private function bootstrapSelf()
    {
        foreach($_SERVER as $key => $value) {
            $this->{$this->toCamelCase($key)} = $value;
        }
    }

    private function toCamelCase($string)
    {
        $result = strtolower($string);
            
        preg_match_all('/_[a-z]/', $result, $matches);
        foreach($matches[0] as $match) {
            $c = str_replace('_', '', strtoupper($match));
            $result = str_replace($match, $c, $result);
        }
        return $result;
    }

    /**
     * Usually for POST, PATCH, and PUT
     */
    public function getBody()
    {
        switch ($this->requestMethod) {
            case "PUT":
                // No break
            case "PATCH":
                // No break
            case "POST":
                return $this->postBodyHandler();
            case "DELETE":
                // No break
            case "GET":
                // No break
            default:
                break;
        }
    }

    private function postBodyHandler()
    {
        // Decode if passed data is json
        if ($this->contentType == 'application/json') {
            $_POST = $this->decodePostData();
        }
        $body = [];
        foreach($_POST as $key => $value) {
            $body[$key] = filter_var(trim($value), FILTER_SANITIZE_SPECIAL_CHARS);
        }
        return $body;
    }

    public function decodePostData()
    {
        $jsonDecoded = json_decode(file_get_contents("php://input"));
        // if not a valid json data
        if (json_last_error() != JSON_ERROR_NONE) {
            return $_POST;
        }
        return $jsonDecoded;
    }
}