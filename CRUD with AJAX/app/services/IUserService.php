<?php

namespace services;

interface IUserService
{
    public function paginateUser($page = 1);
    public function createUser($data);
    public function updateUser($id, $data);
    public function deleteUser($id);
}
