<?php

namespace services;

use models\User;
use helpers\QueryParams;
use helpers\Paginate;

class UserService implements IUserService
{
    private $model;

    public function __construct(User $model = null)
    {
        if ( ! $model) {
            $this->model = new User();
        } else {
            $this->model = $model;
        }
    }

    public function paginateUser($page = 1)
    {
        $paginate = new Paginate(2, $page);
        return $this->model->paginate($paginate);
    }

    public function createUser($data)
    {
        $user = new User();
        $this->handleUserInput($user, $data);
        $user->add()->run();
    }

    public function updateUser($id, $data)
    {
        $user = new User($id);
        $this->handleUserInput($user, $data);
        $user->updateById()->run();
    }

    public function handleUserInput(&$user, $data)
    {
        $user->setName($data['name']);
        $user->setGender($data['gender']);
        $user->setBirthdate($data['birthdate']);
        $user->setEmail($data['email']);
    }

    public function deleteUser($id)
    {
        $user = new User($id);
        $user->deleteById()->run();
    }
}