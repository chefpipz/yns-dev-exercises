<?php

namespace helpers;

class View
{
    public function __construct(ViewLoader $viewLoader)
    {
        $this->viewLoader = $viewLoader;
    }

    public function display($viewName, $data)
    {
        include_once $this->viewLoader->header;
        include_once $this->viewLoader->navbar;
        include_once $this->viewLoader->load($viewName);
        include_once $this->viewLoader->footer;
    }
}