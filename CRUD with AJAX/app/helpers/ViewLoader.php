<?php

namespace helpers;

class ViewLoader
{
    public function __construct($path)
    {
        $this->path = $path;
        $this->layout = $path . 'hoc/layout.php';
        $this->header = $path . 'templates/header.php';
        $this->navbar = $path . 'templates/navbar.php';
        $this->footer = $path . 'templates/footer.php';
    }

    public function load($viewName)
    {
        if ( ! file_exists($this->path.$viewName)) {
            throw new Exception("View does not exist: ". $this->path.$viewName);
        }
        return $this->path.$viewName;
    }
}