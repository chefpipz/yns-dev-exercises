<?php

namespace helpers;

class Paginate
{
    public $limit, $page;

    function __construct($limit, $page = 1)
    {
        $this->limit = $limit;
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getOffset()
    {
        return ($this->page - 1) * $this->limit;
    }
}