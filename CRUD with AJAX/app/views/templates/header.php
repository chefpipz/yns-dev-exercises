<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>5-2</title>

    <!--GLOBAL CSS-->
    <link rel="stylesheet" type="text/css" href="app/styles/app.css">
    <link rel="stylesheet" type="text/css" href="app/styles/button.css">
    <link rel="stylesheet" type="text/css" href="app/styles/card.css">
    <link rel="stylesheet" type="text/css" href="app/styles/form.css">
    <link rel="stylesheet" type="text/css" href="app/styles/modal.css">
    <link rel="stylesheet" type="text/css" href="app/styles/pagination.css">
    <link rel="stylesheet" type="text/css" href="app/styles/table.css">

    <script src="https://kit.fontawesome.com/fa39edb7db.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?="/app/views/utils/xhr-request.js"?>"></script>
    <script type="text/javascript" src="<?="/app/views/utils/pipz.js"?>"></script>
    <script type="text/javascript" src="/app/views/components/widgets/v-modal.js"></script>
</head>
<body>