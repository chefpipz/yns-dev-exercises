class Pipz {
    ERROR_MSG = 'Oops.. Something went wrong';
    renderError ($ref, msg = this.ERROR_MSG) {
        $ref.innerHTML = `
            <div class="center disabled">
            ${msg}
            </div>
        `;
    }

    renderElement(el, innerText) {
        let $element = document.createElement(el);
        $element.innerHTML = innerText;
        return $element;
    }
}

const RENDER_ELEMENT = (componentName, parent = document.body) => {
    const el = document.createElement(componentName);
    parent.appendChild(el);
};

const MODAL = {
    alert: (msg) => {
        MODAL.display(msg);
    },
    success: (msg, callback) => {
        let el = MODAL.display(msg, 'success');
        el.addEventListener('onClose', callback);
    },
    display: (msg, theme='alert') => {
        const existingModal = document.body.querySelector('v-modal');
        if (existingModal) {
            existingModal.remove();
        }
        const el = document.createElement('v-modal');
        document.body.appendChild(el);

        // Call theme first
        el.theme = theme;
        el.header = 'Notice!';
        el.body = `
            <div class="body-alert-text">
                ${msg}
            </div>
        `;
        return el;
    }
}