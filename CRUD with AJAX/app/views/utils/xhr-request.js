const defaultRequestData = {
  method: 'GET',
  headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
  },
  redirect: 'follow',
}
const validStatusCodes = [200, 201, 204];
const inValidStatusCodes = [500, 404, 422];

const XHRRequest = {
    get : async (url) => {
        const response = await fetch(url, defaultRequestData);
        return RESPONSE_HANDLER(response);
    },
    post : async ({url, data}) => {
        const response = await fetch(url, {
            ...defaultRequestData,
            method: 'POST',
            body: JSON.stringify(data),
        })
        return RESPONSE_HANDLER(response);
    },
    delete: async (url) => {
        const response = await fetch(url, {
            ...defaultRequestData,
            method: 'DELETE'
        });
        return RESPONSE_HANDLER(response);
    },
    put: async ({ url, data }) => {
        const response = await fetch(url, {
          ...defaultRequestData,
          method: "PUT",
          body: JSON.stringify(data),
        });
        return RESPONSE_HANDLER(response);
    }
}

async function RESPONSE_HANDLER(response) {
    if (response.status === 400) {
        const errors = await response.json();
        return Promise.reject({ errors });
    }
    
    if ( ! validStatusCodes.includes(response.status)) {
        return Promise.reject();
    }

    if (response.status === 204) {
        return Promise.resolve();
    }

    return await response.json();
}