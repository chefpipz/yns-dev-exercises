customElements.define("v-modal", class extends HTMLElement {

    sizes = {
        sm: 'modal-sm',
        lg: 'modal-lg'
    }
    themes = {
        success: 'modal-success'
    }

    connectedCallback() {
        this.render();
    }

    disconnectedCallback() {
        this._closeBtn.removeEventListener('click', e => { this.dispatchEvent(new Event('onClose', {})) });
        if (this.getAttribute("type") === "submit") {
            this._submitBtn.removeEventListener('click', e => { this.dispatchEvent(new Event('onSubmit', {})) });
        }
    }

    render()
    {
        const getTheme = () => {
            if ( ! this.hasAttribute('theme')) {
                return ''
            }
            console.log(this.themes[this.getAttribute('theme')]);
            return this.themes[this.getAttribute('theme')];
        }
        this.innerHTML = `
            <div class="modal-content ${this.sizes[this.getAttribute("size")]}">
                <div class="modal-header ${getTheme()}">
                    <span class="modal-header-text">
                        ${this.getAttribute("header")}
                    </span>
                    <button
                        type="button"
                        class="modal-close"
                    >
                        <i class="fa fa-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    ${this.innerHTML}
                </div>
                ${this.renderSubmit()}
            </div>
        `;

        this._closeBtn = this.querySelector('.modal-close');
        this._closeBtn.addEventListener('click', e => {
            this.dispatchEvent(new Event('onClose', {}))
            this._close();
        });
        this._headerText = this.querySelector('.modal-header-text');
        this._body = this.querySelector('.modal-body');
        if (this.getAttribute("type") === "submit") {
            this._submitBtn = this.querySelector('.modal-submit');
            this._submitBtn.addEventListener('click', e => { this.dispatchEvent(new Event('onSubmit', {})) });
        }
    }

    static get observedAttributes() {
        return ["size", "type", "header"];
    }

    set header(val) {
        this._headerText.innerHTML = val;
    }

    set body(val) {
        this._body.innerHTML = val;
    }

    set theme(val) {
        this.setAttribute('theme', val);
        this.render();
    }

    renderSubmit() {
        if (this.getAttribute("type") !== "submit") return '';

        return `
            <button class="modal-submit">
                SUBMIT
            </button>
        `;
    }

    _close() {
        const _modalContent = this.querySelector('.modal-content')
        _modalContent.classList.add('hide');
        setTimeout(() => {
            this.parentNode.removeChild(this);
        }, 100);
    }
});