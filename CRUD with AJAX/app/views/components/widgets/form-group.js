const template = document.createElement('template');
template.innerHTML = `
    <style>
        :host {
            display: block
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
        }
        input {
            border: 0;
            box-sizing: border-box;
            outline: none;
            padding: 0.5rem;
            font-size: 0.9rem;
            border-radius: 5px;
            box-shadow: inset 0px 0px 3px rgba(0, 0, 0, 0.25);
            margin: 0.5rem 0 0 0;
            width: 100%;
        }
        .is-invalid {
            border: 1px solid #dd2c00;
        }
        .form-input {
            width: 100%;
        }
        .form-error {
            color: #dd2c00;
            text-align: left;
            text-transform: capitalize;
            letter-spacing: 1px;
            font-size: 0.9rem;
            font-style: italic;
        }

        .form-description {
            text-align: left;
            font-size: 0.9rem;
            font-weight: 400;
            color: #131313;
            margin: 0.2rem 0;
        }
    </style>
    <div class="form-group">
        <div class="form-input">
            <input
                class="input"
                type="text"
                name=""
                value=""
                placeholder=""
            />
        </div>
        <div class="form-description">
            <slot name="description"></slot>
        </div>
        <div class="form-error">
            <span></span>
        </div>
    </div>
`

class FormGroup extends HTMLElement {

    constructor () {
        super();
        this._shadowRoot = this.attachShadow({ mode: 'open' });
        this._shadowRoot.appendChild(template.content.cloneNode(true));

        this._input = this._shadowRoot.querySelector('.input');
        this._error = this._shadowRoot.querySelector('.form-error span');
    }

    connectedCallback() {
        this._input.addEventListener('change', this._handleChange.bind(this));
    }

    disconnectedCallback() {
        this._input.removeEventListener('change', this._handleChange.bind(this));
    }

    _handleChange(event) {
        this.setAttribute('value', event.target.value);
    }

    get value () {
        return this._input.getAttribute('value');
    }

    set value (val) {
        this._input.setAttribute('value', val);
    }

    set error (val) {
        if (val) {
            this._input.classList.add('is-invalid');
            this._error.innerHTML = val;
        } else {
            this._input.classList.remove('is-invalid');
            this._error.innerHTML = '';
        }
    }

    static get observedAttributes () {
        return ['value', 'name', 'placeholder', 'description', 'type'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (oldValue === newValue) return;

        this._input.setAttribute(name, newValue)
    }

}

customElements.define('form-group', FormGroup);