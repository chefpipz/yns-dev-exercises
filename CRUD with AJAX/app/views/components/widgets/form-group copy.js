customElements.define("form-group", class extends HTMLElement {

    static get observedAttributes() {
        return ["name", "placeholder", "error", "description"];
    }

    attributeChangedCallback(name, old, change) {
        console.log(this.name);
    }

    connectedCallback() {
        this.innerHTML = `
            <div class="form-group">
                <input
                    name="${this.getAttribute("name")}"
                    placeholder="${this.getAttribute("placeholder")}"
                    type="text"
                />
                <div class="form-description">
                    ${this.description}
                </div>
                <div class="invalid-feedback name-error">
                    ${this.getAttribute("error")}
                </div>
            </div>
        `;
        this._el = document.querySelector('form-group');
        this._input = this._el.querySelector('input');
        this.addListeners();
    }

    disconnectedCallback() {
        this.removeListeners();
    }

    addListeners() {
    }

    removeListeners() {
    }
});