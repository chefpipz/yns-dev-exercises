customElements.define("v-pager", class extends HTMLElement {

    constructor()
    {
        super();
        this.totalPages = 0;
        this.pageNumber = 1;
        this.nextPage = null;
        this.firstPage = null;
        this.link = "/";
    }

    connectedCallback() {
        this.render();
    }

    render()
    {
        if (this.totalPages <= 1) {
            return;
        }
        this.innerHTML = `
            <div class="pager">
                <span class="prev-page-btns">
                ${(!!this.prevPage)
                    ? `
                        <a
                            class="page first-page"
                            href="${this.link}?page=1">
                            <i class="fa fa-angle-double-left"></i>
                        </a>
                        <a
                            class="page prev-page"
                            href="${this.link}?page=${this.prevPage}">
                            <i class="fa fa-angle-left"></i>
                        </a>
                    `
                    : ''
                }
                </span>
                ${this.renderPages()}
            </div>
        `;
        // this._firstPageBtn = this.querySelector('.first-page');
        // this._prevPageBtn = this.querySelector('.prev-page');
        // this._nextPageBtn = this.querySelector('.next-page');
        // this._lastPageBtn = this.querySelector('.last-page');
        // this._firstPageBtn = this.addEventListener('click', e => {
        //     this.dispatchEvent(new CustomEvent('onChangePage', {'detail': 1}))
        // });
        // this._prevPageBtn = this.addEventListener('click', e => {
        //     this.dispatchEvent(new CustomEvent('onChangePage', {'detail': this.prevPage}))
        // });
        // this._nextPageBtn = this.addEventListener('click', e => {
        //     this.dispatchEvent(new CustomEvent('onChangePage', {'detail': this.nextPage}))
        // });
        // this._lastPageBtn = this.addEventListener('click', e => {
        //     this.dispatchEvent(new CustomEvent('onChangePage', {'detail': this.totalPages}))
        // });
    }

    renderPages()
    {
        let html = '<span class="page-nos">';
        // Number of page number to display
        let start = 1;
        let numberLimit = this.totalPages > 10 ? 10 : this.totalPages;
        let end = numberLimit;
        let hasNext = this.totalPages > numberLimit && !!this.nextPage;
        if (this.pageNumber >= numberLimit) {
            end = this.pageNumber;
            start = this.pageNumber - numberLimit + 1;
        }

        for (let i = start; i <= end; i++) {
            html += `
                <a
                    class="page ${i === this.pageNumber ? 'current': ""}"
                    href="${this.link}?page=${i}">
                    ${i}
                </a>
            `
        }
        if (hasNext) {
            if (end + 1 < this.totalPages) {
                html += `
                    <a
                        class="page"
                        href="${this.link}?page=${end + 1}">
                        ...
                    </a>
                `
            }
            html += `
                <a
                    class="page"
                    href="${this.link}?page=${this.totalPages}">
                    ${this.totalPages}
                </a>
            `
        }
        return html += '</span>';
    }
});