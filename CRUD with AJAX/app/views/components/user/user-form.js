customElements.define("user-form", class extends HTMLElement {
    constructor() {
        super()
        this._formType = "add";
    }

    connectedCallback() {
        this.innerHTML = `
            <v-modal
                header="Add User"
                size="sm"
                type="submit"
            >
                <div class="form-user">
                    <form>
                        <form-group
                            name="name"
                            placeholder="Name"
                        >
                        </form-group>
                        <form-group
                            name="email"
                            placeholder="Email"
                        >
                        </form-group>
                        <div class="form-radio gender">
                            <input type="radio"
                                name="gender"
                                id="gender_M"
                                value="M"
                                />
                            <label>M</label>
                            <input type="radio"
                                name="gender"
                                id="gender_F"
                                value="F"
                                />
                            <label>F</label>
                            <div class="invalid-feedback"></div>
                        </div>
                        <form-group
                            name="birthdate"
                            placeholder="Birthdate"
                            type="date"
                        >
                        </form-group>
                    </form>
                </div>
            </v-modal>
        `;

        this._modal = document.querySelector('v-modal');
        this._name = this._modal.querySelector('form-group[name=name]');
        this._email = this._modal.querySelector('form-group[name=email]');
        this._gender = this._modal.querySelector('input[name=gender]');
        this._genderError = this._modal.querySelector(".form-user .gender .invalid-feedback");
        this._male = this._modal.querySelector('input#gender_M');
        this._female = this._modal.querySelector('input#gender_F');
        this._birthdate = this._modal.querySelector('form-group[name=birthdate]');
        this.addListeners();
    }

    disconnectedCallback() {
        this.removeListeners();
    }

    static get observedAttributes() {
        return ["id", "name", "formType"];
    }

    set name(val) {
        this._name.value = val;
    }

    get gender() {
        if (this._male.checked) {
            return "M";
        }
        if (this._female.checked) {
            return "F";
        }
        return "";
    }

    set gender(val) {
        if (val === "M") {
            this._male.checked = true;
        }
        if (val === "F") {
            this._female.checked = true;
        }
        this._gender.value = val;
    }

    get data() {
        return {
            name: this._name.value,
            email: this._email.value,
            gender: this.gender,
            birthdate: this._birthdate.value
        }
    }

    set email(val) {
        this._email.value = val;
    }

    set birthdate(val) {
        this._birthdate.value = val;
    }

    set formType(val) {
        if (val === "update") {
            this._formType = "update"
            this._modal.header = "Update User";
        } else {
            this.setAttribute("formType", "add");
            this._modal.header = "Add User";
        }
    }

    addListeners() {
        this._modal.addEventListener('onClose', this._close.bind(this));
        this._modal.addEventListener('onSubmit', this._handleSubmit.bind(this));
    }

    removeListeners() {
        this._modal.removeEventListener('onClose', this._close.bind(this));
        this._modal.removeEventListener('onSubmit', this._handleSubmit.bind(this));
    }

    _handleSubmit() {
        if (this._formType === "update") {
            return this._handleUpdateUser();
        }
        return this._handleAddUser();
    }

    _handleUpdateUser() {
        const url = `/api/users?id=${this.id}`;
        this._handleLoading();
        XHRRequest
            .put({ url, data: this.data })
            .then(this._handleSuccess.bind(this))
            .catch(this._handleError.bind(this))
    }

    _handleAddUser() {
        const url = '/api/users/create';
        this._handleLoading();
        XHRRequest
            .post({ url, data: this.data })
            .then(this._handleSuccess.bind(this))
            .catch(this._handleError.bind(this))
    }

    _handleSuccess() {
        let msg = ``;
        if (this.formType === 'update') {
            msg = `User ${this._name.value} was successfully updated`;
        } else {
            msg = `User ${this._name.value} was successfully added`;
        }
        MODAL.success(msg, () => {
            location.reload();
        });
        // this.dispatchEvent(new Event('onSuccess', {}));
        // this._close();
        // location.reload();
        // window.location.href = "/"
    }

    _handleLoading() {
        //TODO add loader
        this._clearErrors();
    }

    _clearErrors() {
        this._name.error = '';
        this._email.error = '';
        this._birthdate.error = '';

        this._genderError.innerHTML = '';
    }

    _handleError(e) {
        if (e.errors.name) {
            this._name.error = e.errors.name;
        }
        if (e.errors.email) {
            this._email.error = e.errors.email;
        }
        if (e.errors.birthdate) {
            this._birthdate.error = e.errors.birthdate;
        }
        if (e.errors.gender) {
            this._genderError.innerHTML = e.errors.gender;
        }
    }

    _close() {
        this.parentNode.removeChild(this);
    }
});