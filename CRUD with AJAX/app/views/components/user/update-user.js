customElements.define("update-user", class extends HTMLElement {

    connectedCallback() {
        this.innerHTML = `
            <v-modal
                header="Update User"
                size="sm"
                type="submit"
            >
                <div class="form-update-user">
                    <form>
                        <form-group
                            name="name"
                            placeholder="Name"
                        >
                        </form-group>
                        <form-group
                            name="email"
                            placeholder="Email"
                        >
                        </form-group>
                        <form-group
                            name="gender"
                            placeholder="Gender"
                        >
                        </form-group>
                        <form-group
                            name="birthdate"
                            placeholder="Birthdate"
                            type="date"
                        >
                        </form-group>
                    </form>
                </div>
            </v-modal>
        `;

        this._modal = document.querySelector('v-modal');
        this._name = this._modal.querySelector('form-group[name=name]');
        this._email = this._modal.querySelector('form-group[name=email]');
        this._gender = this._modal.querySelector('form-group[name=gender]');
        this._birthdate = this._modal.querySelector('form-group[name=birthdate]');
        this.addListeners();
    }

    disconnectedCallback() {
        this.removeListeners();
    }

    static get observedAttributes() {
        return ["id", "name"];
    }

    set name(val) {
        this._name.value = val;
    }

    set gender(val) {
        this._gender.value = val;
    }

    set email(val) {
        this._email.value = val;
    }

    set birthdate(val) {
        this._birthdate.value = val;
    }

    addListeners() {
        this._modal.addEventListener('onClose', this._close.bind(this));
        this._modal.addEventListener('onSubmit', this._handleSubmit.bind(this));
    }

    removeListeners() {
        this._modal.removeEventListener('onClose', this._close.bind(this));
        this._modal.removeEventListener('onSubmit', this._handleSubmit.bind(this));
    }

    _handleSubmit() {
        const url = `/api/users?id=${this.id}`;
        let data = {
            name: this._name.value,
            email: this._email.value,
            gender: this._gender.value,
            birthdate: this._birthdate.value
        }
        XHRRequest
            .put({ url, data })
            .then(() => window.location.href = "/")
            .catch(this._handleError.bind(this))
    }

    _handleLoading() {
        //TODO add loader
        this._clearErrors();
    }

    _clearErrors() {
        this._name.error = '';
        this._email.error = '';
        this._gender.error = '';
        this._birthdate.error = '';
    }

    _handleError(e) {
        if (e.errors) {
            this._name.error = e.errors.name;
            this._email.error = e.errors.email;
            this._gender.error = e.errors.gender;
            this._birthdate.error = e.errors.birthdate;
        }
    }

    _close() {
        this.parentNode.removeChild(this);
    }
});