<?php

namespace models;

use helpers\QueryParams;
use helpers\Paginate;

class Model implements ModelImpl
{
    // Default PK for every Table
    protected $id;
    private static $db;
    private static $result = null; // Holder for query result
    private static $query = null; // For preparation of query

    public function __construct($id=null)
    {
        if ( ! isset(static::$tableName) || empty(static::$tableName)) {
            throw new \InvalidArgumentException("No Table was declared: " . get_class($this));
        }
        if ( ! isset(static::$fieldNames) || empty(static::$fieldNames)) {
            throw new \InvalidArgumentException("No Fieldnames was declared: " . get_class($this));
        }
        if ( ! is_null($id)) {
            $this->setId($id);
        }
        self::$db = \DB::getDb();
    }

    public function add()
    {
        $table = static::$tableName;
        $fields = [];
        $values = [];

        foreach (get_object_vars($this) as $columnName => $columnValue) {
            if ( ! empty($columnValue)) {
                $fields[] = $columnName;
                $values[] = $columnValue;
            }
        }
        self::$query = "INSERT INTO $table (";
        self::$query .= implode(",", $fields);
        self::$query .= ") ";
        self::$query .= "VALUES ('";
        self::$query .= implode("','", $values);
        self::$query .= "') ";

        return $this;
    }

    /**
     * Handles updating of data
     * function needs query params
     * so no mistake in updating all
     * 
     * @param helpers\QueryParams $queryParams
     */
    public function update(QueryParams $queryParams)
    {
        if (empty($queryParams->getWhere())) {
            throw new \Exception("Updating without where clause " . get_class($this));
        }

        $table = static::$tableName;
        self::$query = "UPDATE $table SET ";

        // Readies the sql query
        // Makes an array of SET `email` = 'test@gmail.com'
        $data = [];
        foreach (get_object_vars($this) as $columnName => $columnValue) {
            if ( ! empty($columnValue)) {
                $data[] = "`$columnName` = '$columnValue'";
            }
        }
        self::$query .= implode(',', $data);
        self::$query .= " " . $queryParams->getWhere();

        return $this;
    }

    public function updateById()
    {
        if ( ! $this->id || empty($this->id)) {
            throw new \BadMethodCallException("Cannot update without an id");
        }
        $table = static::$tableName;
        $queryParams = new QueryParams();
        $queryParams->setWhere(["$table.id" => $this->id]);
        $this->update($queryParams);

        return $this;
    }

    /**
     * Handles the deletion of data by the table's id
     */
    public function deleteById()
    {
        if ( ! $this->id || empty($this->id)) {
            throw new \BadMethodCallException("Cannot delete without an id");
        }

        $table = static::$tableName;
        self::$query = "DELETE FROM $table WHERE id = $this->id";

        return $this;
    }

    public function fetch(QueryParams $queryParams=null)
    {
        $table = static::$tableName;
        if ( ! empty($queryParams)) {
            $query = $queryParams->getFields();
            $query .= "FROM $table ";
            $query .= $queryParams->getJoin();
            $query .= $queryParams->getWhere();
            $query .= $queryParams->getOrder();
            $query .= $queryParams->getLimit();
            $query .= $queryParams->getOffset();
        } else {
            $query = "SELECT * ";
            $query .= "FROM $table ";
        }

        $result = self::$db->query($query);
        if ( ! $result) {
            throw new \Exception(self::$db->error);
        }
        self::$result = $result;
        return $this;
    }

    public function fetchById(QueryParams $queryParams=null) {
        if ( ! $this->id || empty($this->id)) {
            throw new \BadMethodCallException("Cannot fetch by id without an ID");
        }
        if (is_null($queryParams)) {
            $queryParams = new QueryParams();
        }
        $queryParams->setWhere(['id' => $this->id]);
        $this->fetch($queryParams);
        return $this;
    }

    public function paginate(Paginate $paginate, QueryParams $queryParams=null)
    {
        $table = static::$tableName;
        $currentPage = $paginate->page;
        if (is_null($queryParams)) {
            $queryParams = new QueryParams();
        }
        // Get data first
        $queryParams->setPagination($paginate);
        $data = $this->fetch($queryParams)->result();

        // Get total number of pages
        $query = "SELECT count(id) as total FROM $table";
        $result = self::$db->query($query);
        if ( ! $result) {
            throw new \Exception(self::$db->error);
        }
        $totalRows = $result->fetch_assoc()['total'];
        $totalPages = ceil($totalRows/$paginate->limit);

        $nextPage = $paginate->getPage() + 1 > $totalPages
            ? null
            : $paginate->getPage() + 1;
        $prevPage = $paginate->getPage() - 1 < 1
            ? null
            : $paginate->getPage() - 1;

        return [
            'data' => $data,
            'totalPages' => $totalPages,
            'pageNumber' => $paginate->page,
            'nextPage' => $nextPage,
            'prevPage' => $prevPage
        ];
    }

    public function result()
    {
        $rows = [];
        while($row = $this->row()) {
            $rows[] = $row;
        }
        return $rows;
    }

    public function row()
    {
        if ( ! self::$result) {
            throw new \BadMethodCallException("Calling this function requires having db->query first");
        }
        return self::$result->fetch_assoc();
    }

    public function run()
    {
        if (empty(self::$query)) {
            throw new \BadMethodCallException("Running an empty sql query");
        }
        $result = self::$db->query(self::$query);
        if ( ! $result) {
            throw new \Exception(self::$db->error);
        }
        return true;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}