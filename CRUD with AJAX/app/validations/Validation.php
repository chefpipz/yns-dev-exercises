<?php

namespace validations;

class Validation implements ValidationImpl
{
    protected $data;

    public $isValid = true;
    public $errors = [];

    function __construct($data)
    {
        $this->data = $data;
        $this->validate();
    }

    public function validate() {}

    /**
     * Checks fields to check
     */
    public function checkEmptyFields($fields)
    {
        foreach($fields as $field) {
            if ( ! isset($this->data[$field]) || empty($this->data[$field])) {
                $this->addError($field, ucfirst($field) . ' is required');
            }
        }
    }

    public static function getValidationData() {
        $errors = [];
        $data = [];
        if (
            isset($_SESSION['errors']) &&
            isset($_SESSION['data']) &&
            ! empty($_SESSION['errors'])
        ) {
            $errors = $_SESSION['errors'];
            $data = $_SESSION['data'];
            unset($_SESSION['errors']);
            unset($_SESSION['data']);
        }
        return [
            'errors' => $errors,
            'data' => $data
        ];
    }

    public static function isValidDate($date)
    {
        return !!strtotime($date);
    }

    /**
     * Add a value of errors
     *
     * @return self
     */ 
    public function addError($key, $value)
    {
        $this->errors[$key] = $value;
        if ($this->isValid) {
            $this->isValid = false;
        }

        return $this;
    }

    public function hasErrors() {
        return ! empty($this->errors);
    }
}
