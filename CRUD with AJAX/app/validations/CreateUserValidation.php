<?php

namespace validations;

use models\User;

class CreateUserValidation extends Validation
{
    /**
     * Required format
     */
    public function validate()
    {
        $this->checkEmptyFields([
            'name',
            'email',
            'gender',
            'birthdate'
        ]);

        if ($this->hasErrors()) return;

        if ( ! filter_var($this->data['email'], FILTER_VALIDATE_EMAIL)) {
            $this->addError('email', 'Please enter a valid email address');
        }

        if ( ! self::isValidDate($this->data['birthdate'])) {
            $this->addError('birthdate', 'Please enter a valid date');
        }

        if ( ! in_array($this->data['gender'], User::GENDERS)) {
            $this->addError('gender', 'Please enter a valid gender');
        }
    }
}