<?php

class DB
{
    private static $db = null;
    const HOST = "127.0.0.1";
    const PORT = "3306";
    const USER_NAME = "user";
    const PASSWORD = "qwe123";
    const DB_NAME = "crud";

    private static $test = 0;

    private function __construct() { }

    public static function getDb($dbName=self::DB_NAME)
    {
        if (is_null(self::$db)) {
            self::$db = new mysqli(
                self::HOST,
                self::USER_NAME,
                self::PASSWORD,
                $dbName,
                self::PORT
            );
        }
        return self::$db;
    }
}