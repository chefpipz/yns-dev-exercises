
export const GET_ERRORS = "GET_ERRORS";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const SET_CURRENT_USER = "SET_CURRENT_USER";

/** Profiles */
export const GET_PROFILE = "GET_PROFILE";
export const SET_PROFILE = "SET_PROFILE";
export const GET_ALL_PROFILES = "GET_ALL_PROFILES";
export const CLEAR_CURRENT_PROFILE = "CLEAR_CURRENT_PROFILE";
export const CLEAR_PROFILES = "CLEAR_PROFILES";

/** TODO Posts */
export const SET_POSTS = "SET_POSTS";
export const TOGGLE_LOADING_POST = "IS_LOADING_POST";
export const ADD_POSTS = "ADD_POSTS";
export const CLEAR_POSTS = "CLEAR_POSTS";
export const GET_PROFILE_POSTS = "GET_PROFILE_POSTS";
/** TODO Like */
/** TODO Comment */
export const GET_POST_COMMENTS = "GET_POST_COMMENTS";
/** TODO Follow */