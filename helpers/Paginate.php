<?php
declare(strict_types=1);

namespace helpers;

class Paginate
{

    public $limit, $page;

    function __construct(int $limit, int $page = 1)
    {
        $this->limit = $limit;
        $this->page = $page;
    }

}