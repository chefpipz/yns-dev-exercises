<?php
declare(strict_types=1);

namespace helpers;

/**
 * Handles all logics for CSV related logics
 */
class CSVHelper
{
    private $filename;
    private $mode;

    public function __construct(String $filename, String $mode = 'w') {
        $this->filename = $filename;
        if ( ! in_array($mode, ['r', 'r+', 'w', 'w+', 'a', 'a+', 'x', 'x+', 'c', 'c+', 'e'])) {
            throw new \InvalidArgumentException('Invalid file mode given: ' . $mode);
        }
        $this->mode = $mode;
    }

    public function add(array $files)
    {
        $handler = fopen($this->filename, 'a');
        foreach ($files as $data) {
            if (is_object($data)) {
                $data = (array) $data;
            }

            fputcsv($handler, $data);
        }
        fclose($handler);
    }

    /**
     * Retrieve a single info by its key
     * @return array
     */
    public function retrieveOne(String $key)
    {
        $handler = file($this->filename);
        return explode(',', $handler[$key]);
    }

    /**
     * Paginates the list of datas from the csv file
     * @return array
     */
    public function paginateList(Paginate $paginate)
    {
        // Check first if file exists
        if ( ! file_exists($this->filename)) return [];
        
        $list = [];
        $handler = file($this->filename);

        // Get the number of data according to given limit
        // TODO convert to while loop so that wont have to check if
        // offset overlaps the handler;
        for ($i=0; $i<$paginate->limit; $i++) {
            $offset = ($paginate->page - 1)*$paginate->limit + $i;
            if ($offset >= count($handler)) break;

            array_push($list, str_getcsv($handler[$offset]));
            $list[$i]['id'] = $offset;
        }

        return [
            'pageCount' => ceil(count($handler)/$paginate->limit),
            'list' => $list
        ];
    }
}