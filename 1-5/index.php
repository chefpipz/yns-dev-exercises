<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>1-5 (FizzBuzz)</title>
</head>
<body>
    <div>
        <form
            action="controller.php"
            method="POST"
        >
            <div class="form-group-input">
                <input
                    type="date"
                    name="date"
                    required
                />
            </div>

            <input type="submit"
                value="Submit"
            />
        </form>

        <?php if (isset($_GET['result']) && isset($_GET['dayOfTheWeek'])): ?>
            <br />
            <div class="result">
                GIVEN = <?=$_GET['given']?>
            </div>
            <div class="result">
                RESULT = <?=$_GET['result']?>
            </div>
            <div class="day-of-the-week">
                DAY OF THE WEEK = <?=$_GET['dayOfTheWeek']?>
            </div>
        <?php endif; ?>
    </div>
</body>
</html>