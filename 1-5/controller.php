<?php
// use helpers\DateTimeHelper;
// TODO: Move to helpers
class DateTimeHelper
{
    /**
     * Gets the string output of the day of the week
     * @param int
     * @return String
     */
    public static function getDayOfTheWeek(int $dateEpoch)
    {
        return date('l', $dateEpoch);
    }

    public static function addDays(String $date, int $nDays)
    {
        return strtotime($date . " + $nDays days");
    }
}

class Controller
{
    public static function main($data)
    {
        $date = $data['date'];
        $newDateEpoch = DateTimeHelper::addDays($date, 3);
        $result = date('Y-m-d', $newDateEpoch);
        $dayOfTheWeek = DateTimeHelper::getDayOfTheWeek($newDateEpoch);
        
        header("Location:index.php?result=$result&dayOfTheWeek=$dayOfTheWeek&given=$date");
    }
}

if ( ! empty($_POST)) {
    Controller::main($_POST);
}