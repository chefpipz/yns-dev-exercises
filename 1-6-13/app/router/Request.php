<?php

require_once "RequestInterface.php";

class Request implements RequestInterface
{
    private $queryStrings = '';

    function __construct()
    {
        $this->bootstrapSelf();
    }

    public function setQueryStrings(String $queryStrings) {
        $this->queryStrings = $queryStrings;
    }
    public function getQueryStrings() { return $this->queryStrings; }

    private function bootstrapSelf()
    {
        foreach($_SERVER as $key => $value) {
            $this->{$this->toCamelCase($key)} = $value;
        }
    }

    private function toCamelCase($string)
    {
        $result = strtolower($string);
            
        preg_match_all('/_[a-z]/', $result, $matches);
        foreach($matches[0] as $match) {
            $c = str_replace('_', '', strtoupper($match));
            $result = str_replace($match, $c, $result);
        }
        return $result;
    }

    /**
     * Usually for POST, PATCH, and PUT
     * TODO: Currently only using GET and POST
     */
    public function getBody()
    {
        switch ($this->requestMethod) {
            case "POST":
                return $this->postBodyHandler();
            case "GET":
                // No break
            default:
                break;
        }
    }

    private function postBodyHandler($data)
    {
        $body = [];
        foreach($_POST as $key => $value) {
            $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }
        return $body;
    }
}