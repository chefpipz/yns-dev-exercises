<?php
use controllers\UsersController;
use controllers\AuthController;

// TODO transfer GET to controller

$ROUTER->get('/', function () use ($VIEW) {
    $VIEW->display('home.php');
});

$ROUTER->get('/users', function () use ($VIEW) {
    $VIEW->display('user-list.php');
});

$ROUTER->get('/create-user', function () use ($VIEW) {
    $VIEW->display('create-user.php');
});

$ROUTER->get('/profile', function () use ($VIEW) {
    // TODO Refractor
    // Display by id if has query string
    if (isset($_GET['id'])) {
        $user = UsersController::getUserById($_GET['id']);
        if ( ! $user) {
            return header("Location:/users");
        }
        $_GET['user'] = $user;
        $VIEW->display('profile.php');
        return;
    }

    // Display own profile if logged in
    if ( ! AuthController::isLoggedIn()) {
        return header("Location:/users");
    }

    $_GET['id'] = $_SESSION['id'];
    $_GET['user'] = UsersController::getUserById($_SESSION['id']);
    $VIEW->display('profile.php');
    return;
});

$ROUTER->get('/upload-image', function () use ($VIEW) {
    $VIEW->display('upload-image.php');
});

$ROUTER->get('/login', function () use ($VIEW) {
    $VIEW->display('login.php');
});

$ROUTER->post('/create-user', 'controllers\UsersController::addUser');
$ROUTER->post('/upload-image', 'controllers\UsersController::addImageToUser');
$ROUTER->post('/login', 'controllers\AuthController::login');
$ROUTER->post('/logout', 'controllers\AuthController::logout');