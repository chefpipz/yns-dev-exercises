<?php

namespace controllers;

use validations\AddUserValidation;
use validations\UploadImageValidation;
use helpers\CSVHelper;
use helpers\FileUploadHelper;
use helpers\HashHelper;
use helpers\Paginate;
use helpers\images\ImageResizerHelper;
use models\User;

class UsersController
{
    public static function addUser()
    {
        $data = $_POST;
        if ( ! self::addUserValidation($data)) {
            return;
        }

        $user = new User(
            $data['name'],
            $data['mobile'],
            $data['email'],
            HashHelper::hashPassword($data['password'])
        );
        $user->add();
        header("Location:/users");
    }

    private static function addUserValidation($data)
    {
        $AddUserValidation = new AddUserValidation($data);

        if ( ! $AddUserValidation->isValid) {
            $_SESSION['errors'] = $AddUserValidation->errors;
            $_SESSION['data'] = $data;
            header("Location:/create-user");
        }

        return $AddUserValidation->isValid;
    }

    public static function addImageToUser()
    {
        $key = $_POST['key'];
        $data = [
            'key' => $key,
            'profile_img' => $_FILES['profile_img']
        ];

        $validation = new UploadImageValidation($data);
        if ( ! $validation->isValid) {
            $_SESSION['errors'] = $validation->errors;
            $_SESSION['data'] = $data;
            return header("Location:/upload-image?id=$key");
        }

        $image = FileUploadHelper::uploadImg(
            BASEPATH . '/app/assets/img/profiles/', // file path
            $file = $data['profile_img'], // file
            $imageName = 'profile_' . $data['key'] // file name
        );
        $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);
        $imageResizer = new ImageResizerHelper("profiles/$imageName.$fileExtension");

        $imageResizer->resizeTo(100, 0, 'maxwidth');
        $imageResizer->saveImage("profiles/$imageName"."_100.png");
        $imageResizer->resizeTo(300, 0, 'maxwidth');
        $imageResizer->saveImage("profiles/$imageName"."_300.png");
        $imageResizer->resizeTo(500, 0, 'maxwidth');
        $imageResizer->saveImage("profiles/$imageName"."_500.png");

        header("Location:/upload-image?id=$key&success_msg=Image Uploaded");
    }

    public static function getUserById($id)
    {
        $csvHelper = new CSVHelper(BASEPATH . '/app/assets/UserInformation.csv');
        // Retrieve the user
        $user = $csvHelper->retrieveOne($id);
        if (empty($user)) {
            return false;
        }

        return new User(
            trim($user[0]),// Name
            trim($user[1]),// Mobile
            trim($user[2]),// Email
            trim($user[3])// Hashed Password
        );
    }

    public static function paginateUsers(Paginate $paginate)
    {
        $csvHelper = new CSVHelper(BASEPATH.'/app/assets/UserInformation.csv');
        return $csvHelper->paginateList($paginate);
    }
}