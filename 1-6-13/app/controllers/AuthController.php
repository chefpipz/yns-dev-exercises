<?php

namespace controllers;

use validations\LoginValidation;
use controllers\UsersController;

class AuthController
{
    public static function login()
    {
        $loginValidation = new LoginValidation($_POST);
        if ( ! $loginValidation->isValid) {
            $_SESSION['errors'] = $loginValidation->errors;
            $_SESSION['data'] = $_POST;
            return header("Location:/login");
        }

        // TODO Add cookies or make token
        $user = UsersController::getUserById($_POST['id']);
        $_SESSION['id'] = $_POST['id'];
        $_SESSION['user'] = [
            "name" => $user->getName(),
            "mobile" => $user->getMobile(),
            "email" => $user->getEmail()
        ];
        header("Location:/");
    }

    public static function isLoggedIn()
    {
        return isset($_SESSION['user']) && ! empty($_SESSION['user']);
    }

    public static function logout()
    {
        // Unset all session
        $_SESSION = [];
        // Remove Cookies along with the session
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        session_destroy();
        header("Location:/login");
    }
}