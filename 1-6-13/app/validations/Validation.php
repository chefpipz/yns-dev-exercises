<?php

namespace validations;

class Validation implements ValidationImpl
{
    protected $data;

    public $isValid = true;
    public $errors = [];

    function __construct($data)
    {
        $this->data = $data;
        $this->validate();
    }

    public function validate() {}
}
