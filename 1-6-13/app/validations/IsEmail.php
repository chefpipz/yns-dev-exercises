<?php

namespace validations;


function isEmail($email) {
    $emailRegex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
    return preg_match($emailRegex, $email) == 1;
}
