<?php

namespace validations;

function isEmpty($data) {
    return ! isset($data) || empty($data);
}
