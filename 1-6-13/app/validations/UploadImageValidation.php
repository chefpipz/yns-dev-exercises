<?php

namespace validations;

class UploadImageValidation extends Validation
{
    public function validate()
    {
        $errors = [];
        if ( ! isset($this->data['key']))
            throw new \InvalidArgumentException("Uploading an image to csv requires a key.");

        if ($this->data['profile_img']['size'] == 0)
            $errors['profile_img'] = 'Please enter an image';

        $this->isValid = empty($errors);
        $this->errors = $errors;
    }
}