<?php
 ;

namespace validations;

use helpers\HashHelper;
use controllers\UsersController;

class LoginValidation extends Validation
{
    public function validate()
    {
        $errors = [];
        if ( ! isset($this->data['id']) || trim($this->data['id']) === "")
            $errors['id'] = 'Please enter your User ID.';

        if ( ! isset($this->data['password']) || empty($this->data['password']))
            $errors['password'] = 'Please enter your password.';
        
        // Make sure everything is not empty first
        // Before proceeding
        if ( ! empty($errors)) {
            $this->isValid = false;
            $this->errors = $errors;
            return;
        }
        
        $user = UsersController::getUserById($this->data['id']);
        if (!$user) {
            $errors['form'] = 'User not found';
        } elseif ( ! HashHelper::doesPasswordMatch($this->data['password'], $user->getPassword())) {
            $errors['form'] = 'ID and Password does not sync';
        }

        $this->isValid = empty($errors);
        $this->errors = $errors;
    }
}