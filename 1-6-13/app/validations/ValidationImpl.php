<?php

namespace validations;

interface ValidationImpl
{
    public function validate();
}