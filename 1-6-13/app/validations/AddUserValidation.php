<?php
 ;

namespace validations;

include_once 'IsEmail.php';
use function validations\isEmail;

/**
 * Validates User Information
 */
class AddUserValidation extends Validation
{
    public function validate()
    {
        $errors = [];
        // Name
        if ( ! isset($this->data['name']) || empty($this->data['name']))
            $errors['name'] = 'Please enter your name.';

        // Mobile
        if ( ! isset($this->data['mobile']) || empty($this->data['mobile']))
            $errors['mobile'] = 'Please enter your mobile number.';
        
        // Email
        if ( ! isset($this->data['email']) || empty($this->data['email']))
            $errors['email'] = 'Please enter your email address.';
        elseif ( ! isEmail($this->data['email']))
            $errors['email'] = 'Please enter a valid email address.';

        // Password
        if ( ! isset($this->data['password']) || empty($this->data['password']))
            $errors['password'] = 'Please enter your password.';
        elseif (strlen($this->data['password']) < 6)
            $errors['password'] = 'Password must contain at least 6 characters.';
        elseif ( ! preg_match("*[0-9]*", $this->data['password']))
            $errors['password'] = 'Password must contain at least 1 numerical value.';
        
        $this->isValid = empty($errors);
        $this->errors = $errors;
    }
}