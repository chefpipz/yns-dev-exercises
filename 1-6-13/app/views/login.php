<?php
// TODO make it as helpers
if (isset($_SESSION['errors']) && ! empty($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    $data = $_SESSION['data'];
    unset($_SESSION['errors']);
    unset($_SESSION['data']);
}
?>
<div class="card card-sm">
    <div class="card-header">
        <p>Login</p>
    </div>
    <form
        action="/login"
        method="POST"
    >
        <?php if (isset($errors['form'])): ?>
            <div class="invalid-feedback">
                <?=$errors['form']?>
            </div>
        <?php endif;?>
        <div class="form-group form-group-input">
            <input
                placeholder="ID"
                type="number"
                name="id"
                value="<?=isset($data['id']) ? $data['id']: ''?>"
            />
        </div>
        <?php if (isset($errors['id'])): ?>
            <div class="invalid-feedback">
                <?=$errors['id']?>
            </div>
        <?php endif;?>

        <div class="form-group form-group-input">
            <input
                placeholder="Password"
                type="password"
                name="password"
                value="<?=isset($data['password']) ? $data['password']: ''?>"
            />
        </div>
        <?php if (isset($errors['password'])): ?>
            <div class="invalid-feedback">
                <?=$errors['password']?>
            </div>
        <?php endif;?>

        <input type="submit"
            value="Submit"
        />
    </form>
</div>