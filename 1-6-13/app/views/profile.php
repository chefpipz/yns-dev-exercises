<style>
    .profile-id {
        font-weight: 500;
    }
    .profile-name {
        text-transform: capitalize;
        font-size: 1.5rem;
        font-weight: 500;
        letter-spacing: 1px;
        color: var(--red-primary);
    }
    .profile-email {
        font-style: italic;
    }
</style>
<div class="card card-sm">
    <div class="card-header">User Profile</div>
    <div class="card-body">
        <div class="profile-name">
            <?=str_replace("\"", "", $_GET['user']->getName())?>
        </div>

        <div class="profile-id">
            User ID #: <?=$_GET['id']?>
        </div>

        <div class="profile-email">
            <?=$_GET['user']->getEmail()?>
        </div>

        <div class="profile-mobile">
            <?=$_GET['user']->getMobile()?>
        </div>
    </div>
</div>