<div class="personal-information">
    <div class="card">
        <?php if (isset($_GET['name'])): ?>
        <div class="name">
            <p><?=$_GET['name']?></p>
        </div>
        <?php endif ?>

        <?php if (isset($_GET['email'])): ?>
        <div class="email">
            <p><?=$_GET['email']?></p>
        </div>
        <?php endif ?>

        <?php if (isset($_GET['mobile'])): ?>
        <div class="mobile">
            <p><?=$_GET['mobile']?></p>
        </div>
        <?php endif ?>
    </div>
</div>