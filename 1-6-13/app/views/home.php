<?php use controllers\AuthController; ?>
<div class="card">
    <div class="card-header">
        HOME
    </div>
    <div class="card-body">
        <h5>
            Hello <?=AuthController::isLoggedIn() ? $_SESSION['user']['name']: 'Guest'; ?>
        </h5>
    </div>
</div>