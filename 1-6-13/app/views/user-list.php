<?php

use helpers\Paginate;
use controllers\UsersController;

$_SERVER['SERVER_NAME'] = $_SERVER['SERVER_NAME'] . '/yns-dev-exercises/1-6-13/';

// Get page number or defaults as 1
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$pageLimit = 10;
$paginate = new Paginate($pageLimit, (int) $page);
$paginatedList = UsersController::paginateUsers($paginate);
$users = $paginatedList['list'];
$pageCount = $paginatedList['pageCount'];

?>
<div>
    <div class="card card-lg">
        <div class="card-header">User List</div>

        <div class="card-body">
            <?php if (count($users) != 0): ?>
                <table class="v-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email Address</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach($users as $user): ?>
                        <?php $id=$user['id']; ?>
                        <tr>
                            <!--ID-->
                            <td>
                                <?=$user['id']?>
                            </td>
                            <!--IMAGE-->
                            <td>
                                <div class="img-rounded img-sm img-center">
                                <!--Check first if the image of the user exists-->
                                <?php if (file_exists(BASEPATH."/app/assets/img/profiles/profile_$id"."_100.png")): ?>
                                    <img src='<?="/app/assets/img/profiles/profile_$id"."_100.png"?>' />
                                <!--Use default img if no image is yet uploaded-->
                                <?php else: ?>
                                    <img src='<?="/app/assets/img/default_avatar.png"?>' />
                                <?php endif ?>
                                </div>
                            </td>

                            <td><?=$user[0]?></td><!--NAME-->
                            <td><?=$user[1]?></td><!--MOBILE-->
                            <td><?=$user[2]?></td><!--EMAIL ADDRESS-->

                            <!--ACTIONS-->
                            <td>
                                <a href="/profile?id=<?=$id?>">
                                    <button class="fab btn-primary">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a href="/upload-image?id=<?=$id?>">
                                    <button class="fab btn-success">
                                        <i class="fa fa-upload"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <?php if ($pageCount > 1): ?>
                    <div class="pager">
                        <!-- Show prev and first button only if not the first page -->
                        <?php if ($paginate->page > 1): ?>
                            <a href="/users?page=1">
                                <<
                            </a>
                            <a href="/users?page=<?=$paginate->page - 1?>">
                                <
                            </a>
                        <?php endif ?>
                        <?php for($i=0; $i<$pageCount; $i++): ?>
                            <?php $pageNo = $i+1; ?>
                            <a
                                class="page <?=$paginate->page == $pageNo ? "current": ""?>"
                                href="/users?page=<?=$pageNo?>">
                                <?=$pageNo?>
                            </a>
                        <?php endfor ?>
                        <!-- Show next and last button only if not last page -->
                        <?php if ($paginate->page < $pageCount): ?>
                            <a href="/users?page=<?=$paginate->page + 1?>">
                                >
                            </a>
                            <a href="/users?page=<?=$pageCount?>">
                                >>
                            </a>
                        <?php endif ?>
                    </div>
                <?php endif ?>

            <?php else: ?>
                <p class="disabled italic">No User/s Found</p>
            <?php endif ?>
        </div>
    </div>
</div>