<?php
use controllers\UsersController;
// use helpers\CSVHelper;
// use models\User;

if (isset($_SESSION['errors']) && ! empty($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    $data = $_SESSION['data'];
    unset($_SESSION['errors']);
    unset($_SESSION['data']);
}

// Redirect to users if id query string is not set
if ( ! isset($_GET['id']) || trim($_GET['id']) == "") {
    return header("Location:/users");
}

$key = $_GET['id'];
$user = UsersController::getUserById($key);
if ( ! $user) {
    return header("Location:/users");
}

?>
<div class="user-list">
    <div class="card">
        <div class="card-header">Add an image </div>
        <div class="card-body">
            <h5>
                <?=str_replace("\"", "", $user->getName())?>
            </h5>
            <p class="small"><?=$user->getMobile()?></p>
            <p class="small italic"><?=$user->getEmail()?></p>

            <?php if(isset($_GET['success_msg'])): ?>
                <div class="success-feedback">
                    <?=$_GET['success_msg']?>
                </div>
            <?php endif ?>

            <form
                action="/upload-image"
                method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <input
                        type="file"
                        name="profile_img"
                        />
                    <?php if (isset($errors['profile_img'])): ?>
                        <div class="invalid-feedback">
                            <?=$errors['profile_img']?>
                        </div>
                    <?php endif;?>
                </div>
                <input
                    type="hidden"
                    name="key"
                    value="<?=$key?>"
                    />
                <input
                    type="submit"
                    value="Submit"
                />
            </form>
        </div>
    </div>
</div>