<?php

namespace helpers;

class Paginate
{

    public $limit, $page;

    function __construct($limit, $page = 1)
    {
        $this->limit = $limit;
        $this->page = $page;
    }

}