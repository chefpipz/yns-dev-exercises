<?php

namespace helpers;

class HashHelper
{
    public static function hashPassword($password)
    {
        return substr(password_hash($password, PASSWORD_DEFAULT), 0, 60);
    }

    public static function doesPasswordMatch($password, $hashedPassword)
    {
        return password_verify($password, $hashedPassword);
    }
}