<?php

namespace models;

use helpers\CSVHelper;

class Model
{
    /**
     * Add one data to table/csv
     */
    public function add()
    {
        if ( ! isset(static::$csv)) {
            throw new \InvalidArgumentException("No CSV file was specified");
        }

        $csvHelper = new CSVHelper(BASEPATH . '/app' . static::$csv, 'a');
        
        return $csvHelper->add([$this]);
    }
}