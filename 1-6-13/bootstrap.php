<?php

require_once('config.php');

require_once('autoload.php');

require_once('app/router/Router.php');
require_once('app/router/Request.php');

require_once('core/ViewLoader.php');
require_once('core/View.php');

spl_autoload_register('loader');
spl_autoload_register('loadNamespace');

$ROUTER = new Router(new Request);
$VIEW = new View(new ViewLoader(BASEPATH . '/app/views/'));