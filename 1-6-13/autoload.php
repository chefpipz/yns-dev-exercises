<?php
function loader($class, $folder = '/') {
    $dir = BASEPATH . '/app' . $folder;

    $class = str_replace("\\", "/", $class);

    if ( ! file_exists($dir.$class.'.php')) return;

    include($dir . $class . '.php');
}
function loadNamespace($class) {
    loader($class);
}
function loadControllers($class) {
    loader($class, '/controllers/');
}
function loadModels($class) {
    loader($class, '/models/');
}
function loadHelpers($class) {
    loader($class, '/helpers/');
}