<?php

$sql = `
    CREATE TABLE IF NOT EXISTS question (
        id int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        description TEXT NOT NULL,
        answer_id tinyint(1) NOT NULL,
    )
`;

$sql = `
    CREATE TABLE IF NOT EXISTS answer (
        id int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        description TEXT NOT NULL,
        question_id int(11) UNSIGNED NOT NULL,
        answer_id tinyint(1) NOT NULL,
        UNIQUE (question_id,answer_id)
    )
`;