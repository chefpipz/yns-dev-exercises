<?php

namespace validations;

interface ValidationImpl
{
    /**
     * Handles the validation part of the system
     * Each with a unique set of validation
     */
    public function validate();

    /**
     * Returns the data and errors made from validation
     */
    public static function getValidationData();
}