<?php

namespace validations;

class Validation implements ValidationImpl
{
    protected $data;

    public $isValid = true;
    public $errors = [];

    function __construct($data)
    {
        $this->data = $data;
        $this->validate();
    }

    public function validate() {}

    public static function getValidationData() {
        $errors = [];
        $data = [];
        if (
            isset($_SESSION['errors']) &&
            isset($_SESSION['data']) &&
            ! empty($_SESSION['errors'])
        ) {
            $errors = $_SESSION['errors'];
            $data = $_SESSION['data'];
            unset($_SESSION['errors']);
            unset($_SESSION['data']);
        }
        return [
            'errors' => $errors,
            'data' => $data
        ];
    }
}
