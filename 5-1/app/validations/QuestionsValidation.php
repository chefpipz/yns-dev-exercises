<?php
 ;

namespace validations;

class QuestionsValidation extends Validation
{

    /**
     * Required format
     * questions => ["{$question_no}" => ['{$question_id}' => '$answer_id']]
     */
    public function validate()
    {
        $errors = [];
        // Questions required to be answerd
        $numberOfQuestions = 10;

        if (count($this->data) != $numberOfQuestions) {
            $errors['form'] = "Please answer all questions";
        }

        $this->isValid = empty($errors);
        $this->errors = $errors;
    }
}