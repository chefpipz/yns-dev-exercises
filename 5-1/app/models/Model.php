<?php

namespace models;

use helpers\QueryParams;

class Model implements ModelImpl
{
    // Default PK for every Table
    protected $id;
    private $db;
    private $result = null; // Holder for query result

    public function __construct()
    {
        if ( ! isset(static::$tableName) || empty(static::$tableName)) {
            throw new \InvalidArgumentException("No Table was declared: " . get_class($this));
        }
        $this->db = \DB::getDb();
    }

    public function fetch(QueryParams $queryParams=null)
    {
        $table = static::$tableName;
        if ( ! empty($queryParams)) {
            $query = $queryParams->getFields();
            $query .= "FROM $table ";
            $query .= $queryParams->getJoin();
            $query .= $queryParams->getWhere();
            $query .= $queryParams->getOrder();
            $query .= $queryParams->getLimit();
        } else {
            $query = "SELECT * ";
            $query .= "FROM $table ";
        }

        $result = $this->db->query($query);
        if ( ! $result) {
            throw new \Exception($this->db->error);
        }
        $this->result = $result;
        return $this;
    }

    public function result()
    {
        $rows = [];
        while($row = $this->row()) {
            $rows[] = $row;
        }
        return $rows;
    }

    public function row()
    {
        if ( ! $this->result) {
            throw new \BadMethodCallException("Calling this function requires having db->query first");
        }
        return $this->result->fetch_assoc();
    }
}