<?php

namespace models;

use models\Answer;
use helpers\QueryParams;

class Question extends Model
{
    protected static $tableName = "question";
}