<?php
// TODO transfer GET to controller

$ROUTER->get('/', function () use ($VIEW) {
    controllers\QuestionsController::index($VIEW);
});
$ROUTER->get('/result', function () use ($VIEW) {
    controllers\QuestionsController::result($VIEW);
});
$ROUTER->post('/questions-submission', 'controllers\QuestionsController::questionSubmission');