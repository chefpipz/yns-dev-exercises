<?php

namespace services;

interface IQuestionService
{
    /**
     * Fetch $n number of random questions
     * along with its choices
     */
    public function fetchQuestionForm($n = 10);

    /**
     * Checks if answer is the correct answer
     * for the given question
     */
    public function isAnswerCorrect($questionId, $answerId);

    /**
     * Calculate the total score of the submitted question form
     */
    public function calculateScore($data);
}