<?php

namespace services;

use models\Question;
use helpers\QueryParams;

class QuestionService implements IQuestionService
{

    private $model;

    public function __construct(Question $model = null) {
        if ( ! $model) {
            $this->model = new Question;
        } else {
            $this->model = $model;
        }
    }

    public function fetchQuestionForm($n = 10)
    {
        $answerService = new \services\AnswersService;
        $queryParams = new QueryParams();
        $queryParams
            // ->setFields('
            //     question.id as question_pk,
            //     question.description as question_description,
            //     answer.id as answer_pk,
            //     answer.description as answer_description,
            //     answer.answer_id as answer_id
            // ')
            // ->addJoin('answer', 'question.id = answer.question_id')
            ->setOrder(['RAND()' => ''])
            ->setLimit($n);
        $questions = $this->model->fetch($queryParams)->result();
        // Retrieve answer choices foreach questions
        foreach ($questions as $key => $question) {
            $questions[$key]['answers'] = $answerService->fetchByQuestion($question['id']);
        }

        return $questions;
    }

    public function calculateScore($data)
    {
        $perfectScore = 10;
        $score = 0;
        foreach ($data as $question) {
            $questionId = key($question);
            $answerId = $question[$questionId];
            if ($this->isAnswerCorrect($questionId, $answerId)) {
                $score++;
            }
        }
        return [
            'score' => $score,
            'remarks' => $this->calculateRemarks($score, $perfectScore)
        ];
    }

    public function isAnswerCorrect($questionId, $answerId)
    {
        $queryParams = new QueryParams();
        $queryParams
            ->setFields('answer_id')
            ->setWhere(['id' => $questionId]);

        $result = $this->model->fetch($queryParams)->row();
        return $result['answer_id'] == $answerId;
    }

    private function calculateRemarks($score, $perfectScore)
    {
        $result = $score/$perfectScore;
        if ($result < 0.5) return "F";
        if ($result <= 0.7) return "C";
        if ($result <= 0.8) return "B";

        return "A";
    }
}