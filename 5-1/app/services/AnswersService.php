<?php

namespace services;

use models\Answer;
use helpers\QueryParams;

class AnswersService implements IAnswersService
{
    private $model;

    public function __construct(Answer $model = null)
    {
        if ( ! $model) {
            $this->model = new Answer();
        } else {
            $this->model = $model;
        }
    }

    public function fetchByQuestion($questionId)
    {
        $queryParams = new QueryParams();
        $queryParams
            ->setFields('id, answer_id, description')
            ->setWhere(['question_id' => $questionId])
            ->setOrder(["RAND()" => ""]);

        return $this->model->fetch($queryParams)->result();
    }
}