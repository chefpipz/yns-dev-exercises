<?php

namespace services;

interface IAnswersService
{
    public function fetchByQuestion($questionId);
}
