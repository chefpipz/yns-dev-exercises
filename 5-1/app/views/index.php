<?php
$validation = validations\Validation::getValidationData();
?>
<style>
.questions {
    padding: 0 2.5rem;
}
.question {
    margin: 1rem 0;
    font-weight: 500;
    font-size: 1.1rem;
}
.answer {
    margin-left: 1rem;
}
</style>
<div class="card card-lg">
    <div class="card-header">Questions</div>
    <div class="card-body">
        <?php if (isset($validation['errors']['form'])): ?>
            <div class="invalid-feedback">
                <h4><?=$validation['errors']['form']?></h4>
            </div>
        <?php endif ?>

        <div class="questions">
            <form
                action="/questions-submission"
                method="POST"
            >
                <?php foreach($data['questions'] as $key => $question): ?>
                    <div class="question">
                        <?=$key + 1?>. <?=$question['description']?>?
                    </div>

                    <?php foreach($question['answers'] as $answer): ?>
                    <?php
                        $question_no = $key + 1;
                        $questionId = $question['id'];
                        $answerId = $answer['id'];
                        $name = "$question_no"."[$questionId]";
                    ?>
                    <div class="answer">
                        <input
                            type="radio"
                            name="<?=$name?>"
                            value="<?=$answer['answer_id']?>"
                            />
                        <?=$answer['description']?>
                    </div>
                    <?php endforeach ?>
                <?php endforeach; ?>
                <input
                    type="submit"
                    />
            </form>
        </div>
    </div>
</div>