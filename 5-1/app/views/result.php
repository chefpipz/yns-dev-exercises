<?php
$score = $_SESSION['score'];
$remarks = $_SESSION['remarks'];
unset($_SESSION['score']);
unset($_SESSION['remarks']);
?>
<style scoped>

.result {
    width: 100%;
    text-align: center;
    text-transform: uppercase;
}
.score {
    font-size: 5rem;
    color: var(--green-primary);
}
.remarks {
    font-size: 2rem;
    font-weight: 500;
    color: var(--green-primary);
}
.score-c, .remarks-c {
    color: #131313;
}
.score-f, .remarks-f {
    color: var(--red-primary);
}
</style>
<div class="card card-sm">
    <div class="card-header">RESULT</div>
    <div class="card-body">
        <div class="result">
            <label>Score:</label>
            <div class="score score-<?=strtolower($remarks)?>">
                <?=$score?>
            </div>
            <label>Remarks:</label>
            <div class="remarks remarks-<?=strtolower($remarks)?>">
                <?=$remarks?>
            </div>
        </div>
    </div>
</div>