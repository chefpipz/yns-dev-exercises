<?php

namespace controllers;

use services\QuestionService;
use validations\QuestionsValidation;

class QuestionsController
{
    private static $question = null;
    private static $answer = null;

    public static function index($VIEW)
    {
        $questionService = new QuestionService();
        $data['questions'] = $questionService->fetchQuestionForm();
        $VIEW->display('index.php', $data);
    }

    public static function questionSubmission()
    {
        $questionService = new QuestionService();
        $result = $questionService->calculateScore($_POST);
        $_SESSION['score'] = $result['score'];
        $_SESSION['remarks'] = $result['remarks'];
        header("Location:/result");
    }

    public static function result($VIEW)
    {
        if ( ! isset($_SESSION['score']) || ! isset($_SESSION['remarks'])) {
            return header("Location:/");
        }

        $VIEW->display('result.php');
    }
}