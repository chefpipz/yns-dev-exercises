<?php

namespace helpers;

class QueryParams
{
    public $fields = "*";
    public $where = null;
    public $order = null;
    public $limit = null;
    public $join = [];

    public function getFields()
    {
        return "SELECT $this->fields ";
    }

    /**
     * Where parameter
     * eg. get recipe where recipe_id = 1
     *          ['recipe_id' => 1]
     * eg. get recipe where in recipe_id = [1, 2, 3]
     *          ['recipe_id' => [1, 2, 3]]
     */
    public function getWhere()
    {
        if (empty($this->where)) return null;

        $query = "WHERE ";
        foreach ($this->where as $key => $value) {
            // Wherein
            if (is_array($value)) {
                $value = implode(",", $value);
                $query = "IN ($value)";
            } else {
                $query .= "$key = $value";
            }
        }
        return $query." ";
    }

    /**
     * Order parameter
     * eg. get recipe ORDER BY id ASC
     *          ['id' => 'ASC']
     */
    public function getOrder()
    {
        if (empty($this->order)) return null;

        $query = 'ORDER BY ';
        foreach ($this->order as $key => $value) {
            $query .= "$key $value";
        }
        return $query." ";
    }

    /**
     * Limit parameter
     * @param int $n - limit number
     */
    public function getLimit()
    {
        if ( ! $this->limit) {
            return null;
        }
        if ( ! is_int($this->limit)) {
            throw new \InvalidArgumentException("LIMIT $this->limit is not a number");
        }
        return "LIMIT $this->limit ";
    }

    /**
     * Retrieves the join query param
     */
    public function getJoin()
    {
        if ( ! $this->join) {
            return null;
        }
        $query = "";
        foreach ($this->join as $value) {
            $query .= $value;
        }
        return $query . " ";
    }

    /**
     * Set the value of fields
     *
     * @return  self
     */ 
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Set the value of where
     *
     * @return  self
     */ 
    public function setWhere($where)
    {
        $this->where = $where;

        return $this;
    }

    /**
     * Set the value of order
     *
     * @return  self
     */ 
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Set the value of limit
     *
     * @return  self
     */ 
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function addJoin($table, $on, $type = "LEFT ")
    {
        $this->join[] = "$type JOIN $table ON $on";

        return $this;
    }
}