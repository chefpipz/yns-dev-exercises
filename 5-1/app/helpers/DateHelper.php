<?php

namespace helpers;

class DateHelper
{

    const MIN_YEAR = 1000;
    const MAX_YEAR = 2100;
    /**
     * Retrieves the number of days in a month
     * @param string $month
     * @param string $year
     * @param string $calendar
     *  eg: 0 - GREGORIAN
     *      1 - JULIAN
     *      2 - JEWISH
     *      3 - FRENCH
     */
    public static function retrieveDaysInMonth($month, $year, $calendar=0)
    {
        return cal_days_in_month($calendar, $month, $year);
    }

    public static function retrieveFirstDayOfTheMonth($month="m", $year="Y")
    {
        return date('N', strtotime(date("01-$month-$year")));
    }

    public static function retrievePrevMonth($month) {
        return $month == 1
            ? 12
            : $month - 1;
    }

    public static function retrieveNextMonth($month) {
        return $month == 12 
            ? 1 
            : $month + 1;
    }

    public static function isValidMonth($month) {
        return $month >= 1 && $month <= 12;
    }

    public static function isValidYear($year) {
        return $year >= self::MIN_YEAR && $year <= self::MAX_YEAR;
    }
}