<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>1-3 (Greatest Common Divisor)</title>
</head>
<body>
    <div>
        <form action="controller.php"
            method="POST">
            <div class="form-group-input">
                <input
                    type="number"
                    name="first_number"
                    required
                />
            </div>

            <div class="form-group-input">
                <input
                    type="number"
                    name="second_number"
                    required
                />
            </div>

            <input type="submit"
                value="Submit"
            />
        </form>
    </div>
</body>
</html>