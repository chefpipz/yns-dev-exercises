<?php

class GreatestCommonDivisor
{
    /**
     * Calculates the Greatest Common Divisor
     * between 2 numbers only
     */
    public static function compute(int $firstN, int $secondN)
    {
        if ($firstN == 0 || $secondN == 0)
            return abs( max(abs($firstN), abs($secondN)) );
       
        $remainder = $firstN % $secondN;

        return ($remainder != 0)
            ? self::gcd($secondN, $remainder)
            : abs($secondN);
    }

    private static function gcd($a, $b) {
        return $b ? self::gcd($b, $a % $b) : $a;
    }
}

class Controller
{
    public static function main()
    {
        echo GreatestCommonDivisor::compute($_POST['first_number'], $_POST['second_number']);
    }
}

if ( ! empty($_POST)) {
    Controller::main();
}

?>