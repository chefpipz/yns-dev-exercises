<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>1-6-13 (Landing Page)</title>

    <!--GLOBAL CSS-->
    <link rel="stylesheet" type="text/css" href="app/styles/app.css">
    <link rel="stylesheet" type="text/css" href="app/styles/button.css">
    <link rel="stylesheet" type="text/css" href="app/styles/card.css">
    <link rel="stylesheet" type="text/css" href="app/styles/form.css">
    <link rel="stylesheet" type="text/css" href="app/styles/image.css">
    <link rel="stylesheet" type="text/css" href="app/styles/pagination.css">
    <link rel="stylesheet" type="text/css" href="app/styles/table.css">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>