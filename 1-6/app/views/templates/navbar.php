<style scoped>
nav.v-navbar {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    padding-right: 1rem;
    background-color: #263238;
    color: #ffffff;

    display: flex;
    justify-content: flex-end;
}

nav.v-navbar .link {
    text-transform: uppercase;
    letter-spacing: 2px;
    font-weight: 400;
    padding: 0.5rem 1rem;
    cursor: pointer;
    background-color: #263238;
    transition: background-color 200ms ease-in-out;
}

nav.v-navbar .link:hover {
    background-color: #ff7961 !important;
}
</style>

<?php use controllers\AuthController; ?>
<nav class="v-navbar">
    <a class="link" href="/">Home</a>

    <?php if (AuthController::isLoggedIn()): ?>
        <a class="link" href="/profile">Profile</a>
    <?php endif ?>

    <a class="link" href="/create-user">Create</a>

    <a class="link" href="/users">Users</a>

    <?php if ( ! AuthController::isLoggedIn()): ?>
        <a class="link" href="/login">Login</a>
    <?php else: ?>
        <form action="/logout" method="POST">
            <button 
                type="submit"
                class="link">
                Logout
            </button>
        </form>
    <?php endif; ?>
</nav>