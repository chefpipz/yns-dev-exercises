<?php
// TODO make it as helpers
if (isset($_SESSION['errors']) && ! empty($_SESSION['errors'])) {
    $errors = $_SESSION['errors'];
    $data = $_SESSION['data'];
    unset($_SESSION['errors']);
    unset($_SESSION['data']);
}
?>
<div class="card card-sm">
    <div class="card-header">Create a User</div>
    <form
        action="/create-user"
        method="POST"
    >
        <div class="form-group form-group-input">
            <input
                placeholder="Name"
                type="text"
                name="name"
                value="<?=isset($data['name']) ? $data['name']: ''?>"
            />
        </div>
        <?php if (isset($errors['name'])): ?>
            <div class="invalid-feedback">
                <?=$errors['name']?>
            </div>
        <?php endif;?>

        <div class="form-group">
            <input
                placeholder="Mobile"
                type="number"
                name="mobile"
                value="<?=isset($data['mobile']) ? $data['mobile']: ''?>"
            />
        </div>
        <?php if (isset($errors['mobile'])): ?>
            <div class="invalid-feedback">
                <?=$errors['mobile']?>
            </div>
        <?php endif;?>

        <div class="form-group form-group-input">
            <input
                placeholder="Email"
                type="text"
                name="email"
                value="<?=isset($data['email']) ? $data['email']: ''?>"
            />
        </div>
        <?php if (isset($errors['email'])): ?>
            <div class="invalid-feedback">
                <?=$errors['email']?>
            </div>
        <?php endif;?>

        <div class="form-group form-group-input">
            <input
                placeholder="Password"
                type="password"
                name="password"
                value="<?=isset($data['password']) ? $data['password']: ''?>"
            />
        </div>
        <?php if (isset($errors['password'])): ?>
            <div class="invalid-feedback">
                <?=$errors['password']?>
            </div>
        <?php endif;?>

        <input type="submit"
            value="Submit"
        />
    </form>
</div>