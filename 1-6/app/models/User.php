<?php
declare(strict_types=1);

namespace models;

class User extends Model
{
    protected $name;
    protected $mobile;
    protected $email;
    protected $password;

    public static $csv = '\\assets\\UserInformation.csv';

    function __construct(
        String $name,
        int $mobile,
        String $email,
        String $password = ""
    ) {
        $this->name = $name;
        $this->mobile = $mobile;
        $this->email = $email;
        $this->password = $password;
    }

    public function toQueryString()
    {
        return "name=$this->name&email=$this->email&mobile=$this->mobile";
    }

    public function getName() { return $this->name; }
    public function getMobile() { return $this->mobile; }
    public function getEmail() { return $this->email; }
    public function getPassword() { return $this->password; }

}