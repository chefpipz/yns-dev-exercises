<?php
declare(strict_types=1);

/**
 * Make a router
 * Add conditional params
 */
class Router
{
    private $request;
    // TODO Only Get and Post for now
    const SUPPORTED_METHODS = [
        "GET",
        "POST"
    ];

    function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    function __call($name, $args)
    {
        list($route, $method) = $args;

        if ( ! in_array(strtoupper($name), self::SUPPORTED_METHODS)) {
            return $this->invalidMethodHandler();
        }

        // Format for $router->get('/link', func)
        $this->{strtolower($name)}[$this->formatRoute($route)] = $method;
    }

    /**
     * Removes trailing forward slashes from the end of the route.
     * Or Adds a single forward slash for empty route
     * EG: $router->get('/test/') = $router->get('/test')
     * $router->get('') = $router->get('/')
     * 
     * @param route - String
     */
    private function formatRoute(String $route)
    {
        $result = rtrim($route, '/');
        if ($result === '') {
            return '/';
        }

        return $this->stripQueryStringAndHashFromPath($result);
    }

    /**
     * Strips away hash and query strings
     */
    function stripQueryStringAndHashFromPath(String $route)
    {
        $splitedRoute = preg_split("/[?#]/", $route);
        return $splitedRoute[0];
    }

    /**
     * Resolves a route
     * Either go to the method or issue the default request handler
     */
    public function resolve()
    {
        $methodList = $this->{strtolower($this->request->requestMethod)};
        $formatedRoute = $this->formatRoute($this->request->requestUri);
        if ( ! isset($methodList[$formatedRoute])) {
            return $this->defaultRequestHandler();
        }
        $method = $methodList[$formatedRoute];

        echo call_user_func_array($method, array($this->request));
    }

    private function invalidMethodHandler()
    {
        header("{$this->request->serverProtocol} 405 Method Not Allowed");
    }

    private function f404RequestHandler()
    {
        header("{$this->request->serverProtocol} 404 Not Found");
    }
        
    private function defaultRequestHandler()
    {
        $this->f404RequestHandler();
    }

    function __destruct()
    {
        $this->resolve();
    }
}