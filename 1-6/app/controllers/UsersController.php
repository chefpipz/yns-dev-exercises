<?php

namespace controllers;

use models\User;

class UsersController
{
    public static function addUser()
    {
        $user = new User(
            $_POST['name'],
            $_POST['mobile'],
            $_POST['email']
        );

        $name = $user->getName();
        $mobile = $user->getMobile();
        $email = $user->getEmail();
        header("Location:/landing?name=$name&mobile=$mobile&email=$email");
    }
}