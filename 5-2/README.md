# 1-6 to 1-13

### Run command on root folder to start php server
```
php -S 127.0.0.1:8000
```
___
## URLs
#### VIEWS
- / - Home Page
- /users - *Displays the list of users (Paginated)*
- /create-user - *Displays the list of users (Paginated)*
- /profile - *Displays own profile (or profile of others , add query string of id eg. /profile?id=2)*
- /upload-image - *Displays the function to upload image for user*
- /login - *Displays the login page*

#### POST
- /create-user - *Creates a User*
- /upload-image - *Uploads an image to the selected user*
- /login - *Login the user and save it to session*
- /logout - *Logout the user*

#### GET (TODO)




