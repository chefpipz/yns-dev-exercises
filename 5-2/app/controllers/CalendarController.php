<?php

namespace controllers;

use helpers\CalendarHelper;
use helpers\DateHelper;

class CalendarController extends Controller
{
    public function index($VIEW)
    {
        if (
            ! isset($_GET['month']) ||
            empty($_GET['month'])
        ) {
            $_GET['month'] = date('m');
        }
        if (
            ! isset($_GET['year']) ||
            empty($_GET['year'])
        ) {
            $_GET['year'] = date('Y');
        }

        if (
            ! DateHelper::isValidMonth($_GET['month']) ||
            ! DateHelper::isValidYear($_GET['year'])
        ) {
            return $VIEW->display('invalid.php');
        }
        // TODO Validation out of bounds and invalid arg
        $calendar = new CalendarHelper($_GET['month'], $_GET['year']);

        $data['calendarDates'] = $calendar->make();
        $data['monthName'] = $calendar->retrieveMonthName();
        $data['currentDay'] = date('d');
        $data['currentMonth'] = date('m');
        $data['currentYear'] = date('Y');

        $calendar->reduceMonth();
        $data['prevMonth'] = $calendar->getMonth();
        $data['prevYear'] = $calendar->getYear();
        $calendar->addMonth(2);
        $data['nextMonth'] = $calendar->getMonth();
        $data['nextYear'] = $calendar->getYear();

        return $VIEW->display('index.php', $data);
    }
}