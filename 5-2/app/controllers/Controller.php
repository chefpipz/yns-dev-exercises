<?php

namespace controllers;

use helpers\View;
use helpers\ViewLoader;

class Controller
{
    protected $VIEW;

    public function __construct($path = BASEPATH . '/app/views/')
    {
        $this->VIEW = new View(new ViewLoader($path));
    }

}