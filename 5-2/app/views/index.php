<div class="card">
    <div class="card-header">Calendar</div>
    <div class="card-body">
        <div class="calendar-month">
            <a 
                class="prev-month"
                href="?month=<?=$data['prevMonth']?>&year=<?=$data['prevYear']?>"
            >
            <
            </a>
            <div class="current-month"><?=$data['monthName']?></div>
            <a
                class="next-month"
                href="?month=<?=$data['nextMonth']?>&year=<?=$data['nextYear']?>"
            >
            >
            </a>
        </div>
        <div class="calendar-header">
            <span class="sunday">Sun</span>
            <span>Mon</span>
            <span>Tue</span>
            <span>Wed</span>
            <span>Thu</span>
            <span>Fri</span>
            <span>Sat</span>
        </div>
    </div>
    <div class="calendar-body">
        <?php foreach($data['calendarDates'] as $week): ?>
        <div class="week">
            <?php foreach($week as $key => $day): ?>
            <div
                class="
                day
                <?=$day['value'] == $data['currentDay'] && $_GET['month'] == $data['currentMonth'] && $_GET['year'] == $data['currentYear'] ? 'current-day': null ?>
                <?=$key == 0 ? 'sunday': null ?>
                <?=$day['disabled'] == 1 ? 'disabled': null ?>
                "
            >
                <?=$day['value']?>
            </div>
            <?php endforeach ?>
        </div>
        <?php endforeach ?>
    </div>
</div>