<?php

namespace helpers;

class CalendarHelper extends DateHelper
{
    private $month, $year, $day, $time;

    const MONTH_NAMES = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    const DAYS_PER_WEEK = 7;
    const CALENDAR_ROWS = 6;

    public function __construct($month, $year, $day="", $time="")
    {
        $this->month = $month;
        $this->year = $year;
        $this->day = $day;
        $this->time = $time;
    }

    public function getMonth() { return $this->month; }
    public function getYear() { return $this->year; }

    /**
     * Generates a 6-columned Calendar
     * calendar[week[day[dayValue, isDisabled]]]
     */
    public function make()
    {
        $previousMonth = static::retrievePrevMonth($this->month);
        $prevYear = $previousMonth == 12 ? $this->year - 1: $this->year;

        $nDaysPrevMonth = static::retrieveDaysInMonth($previousMonth, $prevYear);
        $nDaysReqMonth = static::retrieveDaysInMonth($this->month, $this->year);
        $startDayOfTheMonth = static::retrieveFirstDayOfTheMonth($this->month, $this->year);

        $calendar = [];
        $dayCounter = 1;
        $isDisabled = 0; // Used for css (Disable values that is not in the current month)
        // Process entire month
        for ($week_i=1; $week_i<=self::CALENDAR_ROWS; $week_i++) {
            $calendar[$week_i] = [];
            for ($day_i=0; $day_i<self::DAYS_PER_WEEK; $day_i++) {
                if ($week_i == 1 && $day_i < $startDayOfTheMonth) {
                    // Display prev days of the month
                    if ($day_i != $startDayOfTheMonth) {
                        $calendar[$week_i][] = [
                            'value' => $nDaysPrevMonth - ($startDayOfTheMonth-1) + $day_i,
                            'disabled' => 1
                        ];
                        continue;
                    }
                }
                $calendar[$week_i][] = [
                    'value' => $dayCounter++,
                    'disabled' => $isDisabled
                ];
                if ($dayCounter > $nDaysReqMonth) {
                    $dayCounter = 1;
                    $isDisabled = 1;
                }
            }
        }

        return $calendar;
    }

    public function retrieveMonthName()
    {
        return self::MONTH_NAMES[$this->month - 1];
    }

    public function reduceYear($n = 1)
    {
        $this->year -= $n;
    }

    public function addYear($n = 1)
    {
        $this->year += $n;
    }

    public function reduceMonth()
    {
        if ($this->month == 1) {
            $this->month = 12;
            $this->reduceYear();
            return;
        }

        $this->month -= 1;
    }

    public function addMonth($n=1)
    {
        $hold = $this->month + $n;
        if ($hold > 12) {
            $this->addYear(floor($hold/12));
        }
        $this->month = $hold%12 == 0 ? 12: $hold%12;
    }
}