<?php

class BasicCalculator
{
    private $firstNumber;
    private $secondNumber;
    private $operand;

    public function __construct($firstNumber, $secondNumber, $operand)
    {
        $this->firstNumber = htmlspecialchars($firstNumber);
        $this->secondNumber = htmlspecialchars($secondNumber);
        $this->operand = htmlspecialchars($operand);
    }

    public function compute()
    {
        switch ($this->operand) {
            case '+':
                return $this->add();
            case '-':
                return $this->substract();
            case '*':
                return $this->multiply();
            case '/':
                return $this->divide();
            default:
                throw new InvalidArgumentException('Invalid Operand');
        }
    }

    private function add()
    {
        return $this->firstNumber + $this->secondNumber;
    }

    private function substract()
    {
        return $this->firstNumber - $this->secondNumber;
    }

    private function multiply()
    {
        return $this->firstNumber * $this->secondNumber;
    }

    private function divide()
    {
        if ($this->secondNumber == 0) {
            echo "Cannot set 0 as dividend";
            return;
        }

        return $this->firstNumber / $this->secondNumber;
    }
}

function main()
{
    $basicCalculator = new BasicCalculator(
        $_POST['first_number'],
        $_POST['second_number'],
        $_POST['operand']
    );
    $result = $basicCalculator->compute();
    echo $result;
}

if ( ! empty($_POST)) {
    main();
}

?>